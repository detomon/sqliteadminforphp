<?php

class saAdmin
{

	protected static $navigationItems;

	const messageVarName = 'saMessages';

	public static function addNavigationItem ($href, $name, $delta = 0)
	{
		self :: $navigationItems [$delta][] = array ($href, $name);
	}

	public static function navigationItems ()
	{
		if (self :: $navigationItems == null)
			return array ();

		ksort (self :: $navigationItems);

		$activeItem  = null;
		$navItems    = array ();
		$queryString = getenv ('QUERY_STRING');

		foreach (self :: $navigationItems as $delta => $items) {
			foreach ($items as $item) {
				$href  = substr ($item [0], 1);
				$match = (substr ($queryString, 0, strlen ($href)) == $href);

				if ($match && ($activeItem == null || $href > $activeItem))
					$activeItem = $href;
			}
		}

		foreach (self :: $navigationItems as $delta => $items) {
			foreach ($items as $item) {
				$navItems [$item [0]] = array (
					'name'   => $item [1],
					'delta'  => $delta,
					'active' => (substr ($item [0], 1) == $activeItem),
				);
			}
		}

		return $navItems;
	}

	public static function queryString ($vars)
	{
		if (empty ($vars))
			return '';

		$frags = array ();

		foreach ($vars as $key => $value)
			$frags [] = urlencode ($key) . '=' . urlencode ($value);

		return '?' . implode ('&', $frags);
	}

	protected static function stripslashesRef (& $item)
	{
		$item = stripslashes ($item);
	}

	protected static function trimRef (& $item)
	{
		$item = trim ($item);
	}

	public static function formValue ($name, $type = 'request', $trim = true, $index = null)
	{
		switch ($type) {
			default:
			case 'request':
				$value = isset ($_REQUEST [$name]) ? $_REQUEST [$name] : null;
				break;
			case 'post':
				$value = isset ($_POST [$name]) ? $_POST [$name] : null;
				break;
			case 'get':
				$value = isset ($_GET [$name]) ? $_GET [$name] : null;
				break;
		}

		if (function_exists ('get_magic_quotes_gpc') && get_magic_quotes_gpc ()) {
			if (is_array ($value)) {
				array_walk ($value, 'saAdmin::stripslashesRef');
			} else {
				$value = stripslashes ($value);
			}
		}

		if ($trim) {
			if (is_array ($value)) {
				array_walk ($value, 'saAdmin::trimRef');
			} else {
				$value = trim ($value);
			}
		}

		if ($index !== null && is_array ($value))
			$value = isset ($value [$index]) ? $value [$index] : null;

		return $value;
	}

	public static function textField ($name, $type = 'text', $index = null)
	{
		$value = self :: formValue ($name);
		$name  = htmlspecialchars ($name);

		if ($index !== null) {
			$name = "{$name}[" . htmlspecialchars ($index) . "]";

			if (is_array ($value))
				$value = isset ($value [$index]) ? $value [$index] : null;
		}
		else {
			$value = htmlspecialchars ($value);
		}

		switch ($type) {
			default:
			case 'text':
				return "<input type=\"text\" name=\"{$name}\" value=\"{$value}\" id=\"{$name}\" />";
				break;
			case 'password':
				return "<input type=\"password\" name=\"{$name}\" value=\"{$value}\" id=\"{$name}\" />";
				break;
			case 'area':
				return "<textarea name=\"{$name}\" id=\"{$name}\">{$value}</textarea>";
				break;
		}
	}

	public static function checkboxField ($name, $value, $index = null)
	{
		$current = self :: formValue ($name);

		if ($index !== null) {
			$name = "{$name}[" . htmlspecialchars ($index) . "]";

			if (is_array ($current))
				$current = isset ($current [$index]) ? $current [$index] : null;
		}
		else {
			$current = htmlspecialchars ($current);
		}

		$checked = ($current == $value) ? "checked=\"checked\"" : '';

		return "<input type=\"checkbox\" name=\"{$name}\" value=\"{$value}\" {$checked} id=\"{$name}\" />";
	}

	public static function selectField ($name, $values, $index = null)
	{
		$value = self :: formValue ($name);
		$name  = htmlspecialchars ($name);

		if ($index !== null) {
			$name = "{$name}[" . htmlspecialchars ($index) . "]";

			if (is_array ($value))
				$value = isset ($value [$index]) ? $value [$index] : null;
		}
		else {
			$value = htmlspecialchars ($value);
		}

		$data = "<select name=\"{$name}\" id=\"{$name}\">\n";

		foreach ($values as $key => $text) {
			$key      = htmlspecialchars ($key);
			$text     = htmlspecialchars ($text);
			$selected = ($key == $value) ? "selected=\"selected\"" : '';

			$data .= "	<option value=\"{$key}\" {$selected}>{$text}</option>\n";
		}

		$data .= "</select>";

		return $data;
	}

	public static function hiddenField ($name, $value = null)
	{
		$current = self :: formValue ($name);

		if ($value) {
			$value = htmlspecialchars ($value);
		}
		else {
			$value = htmlspecialchars (saAdmin :: formValue ($name));
		}

		return "<input type=\"hidden\" name=\"{$name}\" value=\"{$value}\" />";
	}


	public static function messages ($type = null)
	{
		saUser :: startSession ();

		$messages = array ();

		if ($type) {
			if (isset ($_SESSION [self :: messageVarName][$type])) {
				$messages = $_SESSION [self :: messageVarName][$type];
				unset ($_SESSION [self :: messageVarName][$type]);
			}
		}
		else if (isset ($_SESSION [self :: messageVarName])) {
			$messages = $_SESSION [self :: messageVarName];
			unset ($_SESSION [self :: messageVarName]);
		}

		return $messages;
	}

	public static function addMessage ($message, $type = 'msg')
	{
		saUser :: startSession ();

		$_SESSION [self :: messageVarName][$type][] = $message;
	}

	public static function errorHandler ($code, $msg, $file, $line)
	{
		saAdmin :: addMessage ($msg . " ({$file}:{$line})", 'err');
	}

	public static function redirect ($url)
	{
		$documentRoot = rtrim ($_SERVER ['DOCUMENT_ROOT'], '/');
		$dirname      = substr (dirname (__FILE__), strlen ($documentRoot));

		header ('Location: http://' . $_SERVER ['SERVER_NAME'] . $dirname . '/' . $url);
		exit;
	}

	public static function normalizeFilePath ($path)
	{
		$tokens = array (
			'%ROOT%'   => rtrim ($_SERVER ['DOCUMENT_ROOT'], '/'),
			'%PARENT%' => dirname (dirname (__FILE__)),
		);

		if (empty ($path))
			return null;

		if ($path [0] != '/') {
			foreach ($tokens as $token => $dir)
				$path = str_replace ($token, "$dir/", $path);

			if ($path [0] != '/')
				return null;
		}

		$path = '/' . implode ('/', array_filter (explode ('/', $path)));

		return $path;
	}

	public static function createDirAtPath ($path)
	{
		$documentRoot = rtrim ($_SERVER ['DOCUMENT_ROOT'], '/');

		if (substr ($path, 0, strlen ($documentRoot)) != $documentRoot)
			return false;

		$relPath = substr ($path, strlen ($documentRoot) + 1);
		$dirs    = array_filter (explode ('/', $relPath));
		$tmpPath = $documentRoot;

		foreach ($dirs as $dir) {
			$tmpPath .= '/' . $dir;

			if (is_dir ($tmpPath) == false) {

				if (@ mkdir ($tmpPath) == false)
					return false;
			}
		}

		return true;
	}

	public static function escapeString ($string)
	{
		$string = str_replace ("'", "''", $string);
		// Other escape ...

		return $string;
	}

}

class saINIFile implements Iterator
{

	protected $_file;
	protected $_ini;

	public static function open ($file)
	{
		$file = realpath ($file);

		if ($file == null)
			return null;

		return new self ($file);
	}

	public function & __get ($name)
	{
		if (isset ($this -> _ini [$name]))
			return $this -> _ini [$name];
	}

	public function __set ($name, $value)
	{
		$this -> _ini [$name] = $value;
	}

	public function save ()
	{
		$file = fopen ($this -> _file, 'w');

		foreach ($this -> _ini as $section => $data) {
			if (is_array ($data)) {
				fprintf ($file, "\n[%s]\n", $section);

				foreach ($data as $key => $value) {
					if ($value !== null)
						fprintf ($file, "%s = %s\n", $key, $value);
				}
			}
			else if ($data !== null) {
				fprintf ($file, "%s = %s\n", $section, $data);
			}
		}

		fclose ($file);
	}

    public function rewind ()
    {
        return reset ($this -> _ini);
    }

    public function current ()
    {
        return current ($this -> _ini);
    }

    public function key ()
    {
        return key ($this -> _ini);
    }

    public function next ()
    {
        next ($this -> _ini);
    }

    public function valid ()
    {
        return key ($this -> _ini);
    }

	public function __construct ($file)
	{
		$this -> _file = $file;
		$this -> _ini  = parse_ini_file ($file, true);
	}

}

class saTable
{

	public    $database;
	protected $columns;
	protected $rowCount;

	public function columns ($withROWID = false)
	{
		if ($this -> columns == null) {
			$this -> columns = array ();

			$result = $this -> database -> connection -> query ("PRAGMA table_info ('{$this -> name}')");

			while ($column = $result -> fetchObject ())
				$this -> columns [$column -> name] = $column;
		}

		if ($withROWID) {
			$row = new stdClass ();
			$row -> name = 'rowid';

			$columns = array_merge (array ($row), $this -> columns);
		}
		else {
			$columns = $this -> columns;
		}

		return $columns;
	}

	public function rowCount ()
	{
		if ($this -> rowCount === null) {
			$result = $this -> database -> connection -> query ("SELECT COUNT(*) FROM '{$this -> name}'");
			list ($count) = $result -> fetch (false);

			$this -> rowCount = $count;
		}

		return $this -> rowCount;
	}

}

class saDatabase
{

	const databaseFile = 'config/databases.conf';

	protected static $list;
	protected static $databases;
	protected $tables;

	public $name;
	public $path;
	public $version;
	public $error;
	public $connection;

	public function connect ()
	{
		$this -> error = null;

		if ($this -> connection == null) {
			$path = saAdmin :: normalizeFilePath ($this -> path);

			$this -> connection = saSQLiteDBWrapper :: open ($path, $this -> version);
			$this -> error      = saSQLiteDBWrapper :: $error;

			if ($this -> connection)
				$this -> version = $this -> connection -> version;
		}
	}

	public function query ($query)
	{
		$this -> connect ();

		return $this -> connection -> query ($query);
	}

	protected static function loadDatabases ()
	{
		if (self :: $list == null) {
			self :: $databases = array ();
			self :: $list      = saINIFile :: open (self :: databaseFile);

			foreach (self :: $list as $name => $info)
				self :: $databases [$name] = new self ($name, $info ['path'], $info ['version']);

			ksort (self :: $databases);
		}
	}

	public static function databases ($name = null)
	{
		self :: loadDatabases ();

		if ($name != null) {
			if (isset (self :: $databases [$name]) == false)
				return null;

			return self :: $databases [$name];
		}

		return self :: $databases;
	}

	public function tables ($name = null)
	{
		$this -> connect ();

		if ($this -> tables == null) {
			$this -> tables = array ();
			$result = $this -> connection -> query ("
				SELECT name
				FROM sqlite_master
				WHERE type = 'table'
				ORDER BY name"
			);

			while ($table = $result -> fetchObject ('saTable')) {
				$table -> database = $this;
				$this -> tables [$table -> name] = $table;
			}
		}

		if ($name != null) {
			if (isset ($this -> tables [$name]) == false)
				return null;

			return $this -> tables [$name];
		}

		return $this -> tables;
	}

	public static function addDatabase ($name, $path, $version = 0)
	{
		self :: loadDatabases ();

		if (isset (self :: $databases [$name])) {
			saAdmin :: addMessage ('An alias with same name already exists', 'err');
			return false;
		}

		if (is_file ($path))
			$path = realpath ($path);

		$database = new self ($name, $path, $version);
		$database -> connect ();

		if (saSQLiteDBWrapper :: $error) {
			saAdmin :: addMessage ("Could not add database: " . saSQLiteDBWrapper :: $error, 'err');
			return false;
		}

		self :: $databases [$name] = $database;
		ksort (self :: $databases);

		self :: $list -> {$name} = array (
			'path'    => $path,
			'version' => $database -> version ? $database -> version : 0,
		);
		self :: $list -> save ();

		return true;
	}

	public function drop ()
	{
		self :: loadDatabases ();

		if (isset (self :: $databases [$this -> name]) == false)
			return false;

		unset (self :: $databases [$this -> name]);

		self :: $list -> {$this -> name} = null;
		self :: $list -> save ();

		return true;
	}

	public function __construct ($name, $path, $version)
	{
		$this -> name    = $name;
		$this -> path    = $path;
		$this -> version = $version;
	}

}

class saUser
{

	const userFile    = 'config/users.conf';
	const sessionUser = 'sqliteAdminUser';

	protected static $users;

	protected static function loadUsers ()
	{
		if (self :: $users == null)
			self :: $users = parse_ini_file (self :: userFile);
	}

	public static function startSession ()
	{
		if (session_id () == '')
			session_start ();
	}

	protected static function validateLogin ($name, $password)
	{
		self :: loadUsers ();

		if (isset (self :: $users [$name])) {
			$password      = md5 ($password);
			$matchPassword = self :: $users [$name];

			if (substr ($matchPassword, 0, 4) == 'md5:') {
				$matchPassword = substr ($matchPassword, 4);
			}
			else {
				$matchPassword = md5 ($matchPassword);
			}

			return ($password == $matchPassword);
		}

		return false;
	}

	public static function login ($name, $password)
	{
		if (self :: validateLogin ($name, $password)) {
			self :: startSession ();

			$_SESSION [self :: sessionUser] = $name;

			return true;
		}

		return false;
	}

	public static function logout ()
	{
		self :: startSession ();

		unset ($_SESSION [self :: sessionUser]);
	}

	public static function isLoggedIn ()
	{
		self :: startSession ();

		return isset ($_SESSION [self :: sessionUser]);
	}

}

class saSQLiteDBWrapper
{

	protected static $versions = array (
		2 => 'SQLiteDatabase,saSQLiteDBWrapper,saSQLiteResultWrapper',
		3 => 'SQLite3,saSQLite3DBWrapper,saSQLite3ResultWrapper',
	);

	protected static $fileHeaders = array (
		2 => '** This file contains an SQLite 2.1 database',
		3 => 'SQLite format 3',
	);

	protected static $validVersions;
	public    static $error;

	protected $database;
	public    $file;
	public    $version;

	public static function checkFileVersion ($file)
	{
		$version = 0;
		$file    = fopen ($file, 'r');

		foreach (self :: $fileHeaders as $v => $header) {
			fseek ($file, 0);
			$fileHeader = fread ($file, strlen ($header));

			if ($fileHeader == $header) {
				$version = $v;
				break;
			}
		}

		fclose ($file);

		return $version;
	}

	public static function checkSystemVersions ($matchVersion = null)
	{
		if (self :: $validVersions == null) {
			self :: $validVersions = array ();

			foreach (self :: $versions as $version => $class) {
				$classes = explode (',', $class);

				if (class_exists ($classes [0]))
					self :: $validVersions [$version] = $classes;
			}
		}

		if ($matchVersion)
			return isset (self :: $validVersions [$matchVersion]);

		return self :: $validVersions;
	}

	public static function open ($file, $useVersion = 0)
	{
		$version = 0;

		if ($useVersion && self :: checkSystemVersions ($useVersion) == false) {
			self :: $error = 'System does not support version';
			return null;
		}

		if (is_file ($file)) {
			$version = self :: checkFileVersion ($file);

			if ($version == 0) {
				if (filesize ($file) > 0) {
					self :: $error = 'File seems not to be a valid database';
					return null;
				}
				else if ($useVersion == 0) {
					self :: $error = 'File version could not be determined';
					return null;
				}
			}
			else if (self :: checkSystemVersions ($version) == false) {
				self :: $error = 'File version is not supported by system';
				return null;
			}
		}

		if ($version == 0) {
			if ($useVersion) {
				$version = $useVersion;
			}
			else {
				// Latest system supported version
				$validVersion = array_slice (self :: checkSystemVersions (), -1, 1, true);
				$version = key ($validVersion);
			}
		}

		//try {
			$classes = self :: $validVersions [$version];

			return new $classes [1] ($file, $version);
		//}
		//catch (Exception $e) {
		//	self :: $error = $e -> getMessage ();
		//	return null;
		//}
	}

	public function query ($query)
	{
		$result = @ $this -> database -> query ($query);
		$class  = self :: $validVersions [$this -> version] [2];

		if ($result == null)
			return null;

		return new $class ($result, $this -> version);
	}

	public function lastInsertRowID ()
	{
		return $this -> database -> lastInsertRowid ();
	}

	public function lastError (& $code = null)
	{
		$code = $this -> database -> lastError ();

		return sqlite_error_string ($code);
	}

	public function __construct ($file, $version)
	{
		$classes = self :: $validVersions [$version];
		$this -> database = new $classes [0] ($file);

		$this -> file    = $file;
		$this -> version = $version;
	}

}

class saSQLite3DBWrapper extends saSQLiteDBWrapper
{

	public function lastInsertRowID ()
	{
		return $this -> database -> lastInsertRowID ();
	}

	public function lastError (& $code = null)
	{
		$code = $this -> database -> lastErrorCode ();

		return $this -> database -> lastErrorMsg ();
	}

}

class saSQLiteResultWrapper
{

	protected $result;
	protected $fields;
	public    $version;
	protected $rowCount;

	public function fields ()
	{
		if ($this -> fields == null) {
			$this -> fields = array ();

			for ($i = 0; $i < $this -> result -> numFields (); $i ++)
				$this -> fields [] = $this -> result -> fieldName ($i);
		}

		return $this -> fields;
	}

	public function rowCount ()
	{
		if ($this -> rowCount === null)
			$this -> rowCount = $this -> result -> numRows ();

		return $this -> rowCount;
	}

	public function fetch ($assoc = true)
	{
		if ($assoc) {
			return $this -> result -> fetch (SQLITE_ASSOC);
		} else {
			return $this -> result -> fetch (SQLITE_NUM);
		}
	}

	public function fetchObject ($class = null)
	{
		if ($class == null)
			$class = 'stdClass';

		if ($row = $this -> fetch (true)) {
			$object = new $class;

			foreach ($row as $field => $value)
				$object -> {$field} = $value;

			return $object;
		}

		return null;
	}

	public function reset ()
	{
		$this -> result -> rewind ();
	}

	public function __construct ($result, $version)
	{
		$this -> result  = $result;
		$this -> version = $version;
	}

}

class saSQLite3ResultWrapper extends saSQLiteResultWrapper
{

	public function fields ()
	{
		if ($this -> fields == null) {
			$this -> fields = array ();

			for ($i = 0; $i < $this -> result -> numColumns (); $i ++)
				$this -> fields [] = $this -> result -> columnName ($i);
		}

		return $this -> fields;
	}

	public function rowCount ()
	{
		if ($this -> rowCount === null) {
			$this -> rowCount = 0;

			while ($this -> result -> fetchArray (SQLITE3_NUM))
				$this -> rowCount ++;

			$this -> result -> reset ();
		}

		return $this -> rowCount;
	}

	public function fetch ($assoc = true)
	{
		if ($assoc) {
			return $this -> result -> fetchArray (SQLITE3_ASSOC);
		} else {
			return $this -> result -> fetchArray (SQLITE3_NUM);
		}
	}

	public function fetchObject ($class = null)
	{
		if ($class == null)
			$class = 'stdClass';

		if ($row = $this -> fetch (true)) {
			$object = new $class;

			foreach ($row as $field => $value)
				$object -> {$field} = $value;

			return $object;
		}

		return null;
	}

	public function reset ()
	{
		$this -> result -> reset ();
	}

}
