$(function () {
	$("td, th").each(function () {
		var $checkbox = $(this).find("input[type=checkbox]");

		$checkbox.click(function (e) {
			e.stopPropagation();
		});

		$(this).click(function () {
			if ($checkbox.prop("checked")) {
				$checkbox.prop("checked", false);
			}
			else {
				$checkbox.prop("checked", true);
			}
		});
	});
});
