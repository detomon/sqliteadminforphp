<?php

$result = null;

if ($_POST) {
	if ($selection = saAdmin :: formValue ('selection')) {
		foreach ($selection as $name) {
			switch (saAdmin :: formValue ('action')) {
			case 'empty':
				$database -> query ("DELETE FROM '" . saAdmin :: escapeString ($name) . "'");

				if ($database -> version >= 3)
					$database -> query ("DELETE FROM sqlite_sequence WHERE name = '" . saAdmin :: escapeString ($name) . "'");

				saAdmin :: addMessage ("Table '$name' has been emptied", 'msg');

				break;
			case 'drop':
				$database -> query ("DROP TABLE '" . saAdmin :: escapeString ($name) . "'");

				saAdmin :: addMessage ("Table '$name' has been dropped", 'msg');

				break;
			}
		}
	}

	if ($action == 'query') {
		$result = $database -> query (saAdmin :: formValue ('query', 'post'));
	}

	// Reset select
	unset ($_REQUEST ['action']);
}

if ($action == 'drop') {
	if ($database -> drop ()) {
		saAdmin :: addMessage ("Alias '{$database -> name}' has been deleted", 'msg');
		saAdmin :: redirect ('');
	}
}

saAdmin :: addNavigationItem ("?alias={$database -> name}", 'Tables');
saAdmin :: addNavigationItem ("?alias={$database -> name}&do=query", 'Query');
saAdmin :: addNavigationItem ("?alias={$database -> name}&do=options", 'Options');
saAdmin :: addNavigationItem ("?alias={$database -> name}&do=drop", 'Delete alias', 100);

$tables = $database -> tables ();

$filePath     = saAdmin :: normalizeFilePath ($database -> path);
$documentRoot = rtrim (getenv ('DOCUMENT_ROOT'), '/');
$filePath     = substr ($filePath, strlen ($documentRoot));

?>
			<h2><?php echo htmlspecialchars ($database -> name); ?> (<?php echo htmlspecialchars ($filePath); ?>)</h2>
<?php if ($action == ''): ?>
			<form action="<?php echo htmlspecialchars ("?alias={$database -> name}"); ?>" method="post">
				<table>
					<colgroup>
						<col />
						<col width="100" />
						<col width="75" />
						<col width="50" />
					</colgroup>
					<caption>Tables (<?php echo count ($tables); ?>)</caption>
					<tr>
						<th>Name</th>
						<th class="number"># Columns</th>
						<th class="number"># Rows</th>
						<th></th>
					</tr>
<?php 	foreach ($tables as $name => $tbl): ?>
					<tr>
						<td><a href="<?php echo htmlspecialchars ("?alias={$database -> name}&table=$name") ?>"><?php echo htmlspecialchars ($name); ?></a></td>
						<td class="number"><?php echo count ($tbl -> columns ()); ?></td>
						<td class="number"><?php echo $tbl -> rowCount (); ?></td>
						<td class="center"><?php echo saAdmin :: checkboxField ("selection[]", $tbl -> name) ?></td>
					</tr>
<?php 	endforeach; ?>
				</table>
				<div class="submit">
					<?php echo saAdmin :: selectField ('action', array ('' => 'Action', 'empty' => 'Empty selected tables', 'drop' => 'Drop selected tables')) ?>
					<input type="submit" value="Apply …" />
				</div>
			</form>
			<form action="<?php echo htmlspecialchars ("?alias={$database -> name}&do=add-table"); ?>" method="post">
				<table>
					<colgroup>
						<col />
						<col width="100" />
						<col width="125" />
					</colgroup>
					<caption>New table</caption>
					<tr>
						<th><label for="name">Name</label></th>
						<th class="number"><label for="colcount"># Columns</label></th>
						<th></th>
					</tr>
					<tr>
						<td><?php echo saAdmin :: textField ('name'); ?></td>
						<td class="number"><?php echo saAdmin :: textField ('colcount'); ?></td>
						<td></td>
					</tr>
				</table>
				<div class="submit">
					<input type="submit" value="Add …" />
				</div>
			</form>
<?php elseif ($action == 'query'): ?>
			<form action="<?php echo htmlspecialchars ("?alias={$database -> name}&do=query"); ?>" method="post">
				<table>
					<colgroup>
						<col />
					</colgroup>
					<caption>Query</caption>
					<tr>
						<td><?php echo saAdmin :: textField ('query', 'area'); ?></td>
					</tr>
				</table>
				<div class="submit">
					<input type="submit" value="Execute" />
				</div>
			</form>
<?php 	if ($result): ?>
			<form action="<?php echo saAdmin :: queryString (array ('alias' => $database -> name)); ?>" method="post">
				<table>
					<caption>Rows (<?php echo $result -> rowCount (); ?>)</caption>
					<tr>
<?php 		foreach ($result -> fields () as $field): ?>
						<th><?php echo htmlspecialchars ($field); ?></th>
<?php 		endforeach; ?>
					</tr>
<?php 		if ($result -> rowCount ()): ?>
<?php 			while ($row = $result -> fetch (true)): ?>
					<tr>
<?php 				$rowid = 0; foreach ($row as $name => $value): if ($name == 'rowid') $rowid = $value; ?>
						<td><?php echo htmlspecialchars ($value); ?></td>
<?php 				endforeach; ?>
					</tr>
<?php 			endwhile; ?>
<?php 		else: ?>
					<td colspan="<?php echo count ($result -> fields ()) + 1; ?>">No rows</td>
<?php 		endif; ?>
				</table>
<?php 	endif; ?>
<?php elseif ($action == 'options'): ?>
			<p>No options available yet</p>
<?php endif; ?>