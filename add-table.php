<?php

$name     = saAdmin :: formValue ('name');
$colcount = (int) saAdmin :: formValue ('colcount');

$types = array (
	'INTEGER'       => 'INTEGER',
	'REAL'          => 'REAL',
	'TEXT'          => 'TEXT',
	'BLOB'          => 'BLOB',
	'AUTOINCREMENT' => 'AUTOINCREMENT',
);

$defaultValues = array (
	''                  => '',
	'NULL'              => 'NULL',
	'CURRENT_TIME'      => 'CURRENT_TIME',
	'CURRENT_DATE'      => 'CURRENT_DATE',
	'CURRENT_TIMESTAMP' => 'CURRENT_TIMESTAMP',
);

function addTable ($database, $name)
{
	global $defaultValues;

	$columns        = array ();
	$primaryColumns = array ();

	foreach (saAdmin :: formValue ('colname') as $i => $colname) {
		$type       = saAdmin :: formValue ('type', 'post', true, $i);
		$isnull     = saAdmin :: formValue ('isnull', 'post', true, $i);
		$dflt_value = saAdmin :: formValue ('dflt_value', 'post', true, $i);
		$primary    = saAdmin :: formValue ('primary', 'post', true, $i);

		if ($colname) {
			if (isset ($columns [$colname])) {
				saAdmin :: addMessage ("Duplicate column '$colname'", 'err');
				return false;
			}

			if ($type == 'AUTOINCREMENT') {
				if ($database -> version >= 3) {
					$type = 'INTEGER PRIMARY KEY AUTOINCREMENT';
				}
				else {
					$type = 'INTEGER PRIMARY KEY';
				}

				$primary = null;
				$isnull  = true;
			}

			if (in_array ($dflt_value, $defaultValues) == false) {
				if (substr ($dflt_value, 0, 1) == '$') {
					$dflt_value = substr ($dflt_value, 1);
				} else {
					if (substr ($dflt_value, 0, 2) == '\$')
						$dflt_value = substr ($dflt_value, 1);
					$dflt_value = "'" . saAdmin :: escapeString ($dflt_value) . "'";
				}
			}

			$definition = "'" . saAdmin :: escapeString ($colname) . "' " . $type;

			if ($primary)
				$primaryColumns [] = "'" . saAdmin :: escapeString ($colname) . "'";
			if ($isnull == null)
				$definition .= " NOT NULL";
			if ($dflt_value)
				$definition .= " DEFAULT $dflt_value";

			$columns [$colname] = $definition;
		}
	}

	if (empty ($columns)) {
		saAdmin :: addMessage ("Empty column names", 'err');
		return false;
	}

	$query = "CREATE TABLE '" . saAdmin :: escapeString ($name) . "' (";
	$query .= implode (', ', $columns);
	if ($primaryColumns)
		$query .= ', PRIMARY KEY (' . implode (', ', $primaryColumns) . ')';
	$query .= ")";

	$database -> query ($query);
	$errstr = $database -> connection -> lastError ($errno);

	if ($errno) {
		saAdmin :: addMessage ("Could not create table: $errstr", 'err');
		return false;
	}

	return true;
}

if ($_POST) {
	if (saAdmin :: formValue ('colname', 'post')) {
		if (addTable ($database, $name)) {
			saAdmin :: addMessage ("Table '{$name}' has been created", 'msg');
			saAdmin :: redirect ("?alias={$database -> name}&table=$name");
		}
	}
	else if ($colcount == 0) {
		saAdmin :: addMessage ('At least 1 colums is required', 'err');
		saAdmin :: redirect (saAdmin :: queryString (array ('alias' => $database -> name, 'name' => $name)));
	}
}

?>
			<h2>
				<a href="<?php echo saAdmin :: queryString (array ('alias' => $database -> name)) ?>"><?php echo htmlspecialchars ($database -> name); ?></a> /
				<span>Add table</span>
			</h2>
			<form action="<?php echo saAdmin :: queryString (array ('alias' => $database -> name, 'do' => 'add-table')) ?>" method="post">
				<table>
					<colgroup>
						<col />
						<col width="100" />
						<col width="55" />
						<col width="150" />
						<col width="100" />
					</colgroup>
					<caption><?php echo saAdmin :: formValue ('name'); ?></caption>
					<tr>
						<th>Name</th>
						<th>Type</th>
						<th>NULL</th>
						<th>Default value</th>
						<th>Primary key</th>
					</tr>
<?php for ($i = 0; $i < $colcount; $i ++): ?>
					<tr>
						<td><?php echo saAdmin :: textField ('colname', 'text', $i); ?></td>
						<td><?php echo saAdmin :: selectField ('type', $types, $i); ?></td>
						<td class="center"><?php echo saAdmin :: checkboxField ('isnull', '1', $i); ?></td>
						<td><?php echo saAdmin :: textField ('dflt_value', 'text', $i); ?></td>
						<td><?php echo saAdmin :: checkboxField ('primary', '1', $i); ?></td>
					</tr>
<?php endfor; ?>
				</table>
				<div class="submit">
					<?php echo saAdmin :: hiddenField ('name') ?>

					<?php echo saAdmin :: hiddenField ('colcount') ?>

					<input type="submit" value="Add " />
				</div>
			</form>
