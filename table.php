<?php

$types = array (
	'INTEGER'       => 'INTEGER',
	'REAL'          => 'REAL',
	'TEXT'          => 'TEXT',
	'BLOB'          => 'BLOB',
	'AUTOINCREMENT' => 'AUTOINCREMENT',
);

$indexes = array (
	'INDEX'  => 'INDEX',
	'UNIQUE' => 'UNIQUE',
);

$defaultValues = array (
	''                 => '',
	'NULL'             => 'NULL',
	'CURRENT_TIME'     => 'CURRENT_TIME',
	'CURRENT_DATE'     => 'CURRENT_DATE',
	'CURRENT_DATETIME' => 'CURRENT_DATETIME',
);

if ($_POST) {
	$selection = saAdmin :: formValue ('selection');

	if (!$selection) {
		$selection = array ();
	}

	switch (saAdmin :: formValue ('action')) {
		case 'add-row':
			$row = $_POST ['values'];

			$values  = array ();
			$columns = array ();

			foreach ($row as $field => $value) {
				if ($value) {
					$values [$field] = "'" . saAdmin :: escapeString ($value) . "'";
				}
			}

			foreach ($row as $field => $value) {
				if (isset ($values [$field])) {
					$columns [$field] = "'" . saAdmin :: escapeString ($field) . "'";
				}
			}

			$values  = implode (', ', $values);
			$columns = implode (', ', $columns);

			$query = "INSERT INTO '" . saAdmin :: escapeString ($table -> name) . "' ($columns)
			VALUES ($values)";

			$database -> query ($query);

			break;
		case 'drop-row':
			foreach ($selection as $id) {
				$database -> query ("
					DELETE FROM '" . saAdmin :: escapeString ($table -> name) . "'
					WHERE rowid = '" . saAdmin :: escapeString ($id) . "'"
				);
			}
			saAdmin :: addMessage ("Row with ROWID $id has been dropped", 'err');
			break;
		case 'drop-row':
			saAdmin :: addMessage ("Adding columns is not yet implemented!", 'err');
			break;
		case 'drop-column':
			saAdmin :: addMessage ("Dropping columns is not yet implemented!", 'err');
			break;
		case 'add-index':
			$name    = $_POST ['name'];
			$index   = $_POST ['index'];
			$columns = $_POST ['columns'];

			$types = array (
				'INDEX'  => '',
				'UNIQUE' => 'UNIQUE',
			);

			$type = $types [$index];
			$database -> query ("
				CREATE $type INDEX '" . saAdmin :: escapeString ($name) . "'
				ON '" . saAdmin :: escapeString ($table -> name) . "'
				($columns)"
			);

			unset ($_REQUEST ['name']);
			unset ($_REQUEST ['index']);
			unset ($_REQUEST ['columns']);

			break;
		case 'drop-index':
			foreach ($selection as $name) {
				$database -> query ("DROP INDEX '" . saAdmin :: escapeString ($name) . "'");
				saAdmin :: addMessage ("Index '$name' has been dropped", 'err');
			}
			break;
	}

	// Reset selection
	unset ($_REQUEST ['selection']);
	unset ($_REQUEST ['action']);
}

saAdmin :: addNavigationItem (saAdmin :: queryString (array ('alias' => $database -> name, 'table' => $table -> name)), 'Browse');
saAdmin :: addNavigationItem (saAdmin :: queryString (array ('alias' => $database -> name, 'table' => $table -> name, 'do' => 'structure')), 'Structure');
saAdmin :: addNavigationItem (saAdmin :: queryString (array ('alias' => $database -> name, 'table' => $table -> name, 'do' => 'indexes')), 'Indexes');
saAdmin :: addNavigationItem (saAdmin :: queryString (array ('alias' => $database -> name, 'table' => $table -> name, 'do' => 'options')), 'Options');

?>
			<h2>
				<a href="<?php echo saAdmin :: queryString (array ('alias' => $database -> name)) ?>"><?php echo htmlspecialchars ($database -> name); ?></a> /
				<span><?php echo htmlspecialchars ($table -> name); ?></span>
			</h2>
<?php if ($action == ''): ?>
<?php
	$fields = $table -> columns (true);
	$primaryCol = false;
	foreach ($fields as $field) {
		if (isset ($field -> pk) && $field -> pk) {
			$primaryCol = $field;
			break;
		}
	}
?>
<?php $rows = $database -> query ("SELECT rowid, * FROM {$table -> name} ORDER BY rowid"); ?>
			<form action="<?php echo saAdmin :: queryString (array ('alias' => $database -> name, 'table' => $table -> name)); ?>" method="post">
				<table>
					<colgroup>
						<col width="50" />
					</colgroup>
					<caption>Rows (<?php echo $rows -> rowCount (); ?>)</caption>
					<tr>
<?php foreach ($fields as $field): ?>
						<th><?php echo htmlspecialchars ($field -> name); ?></th>
<?php endforeach; ?>
						<th width="50"></th>
					</tr>
<?php if ($rows -> rowCount ()):
	$idFieldName = $primaryCol ? $primaryCol -> name : 'rowid';
?>
<?php 	while ($row = $rows -> fetch (true)):
			$rowid = $row [$idFieldName];
?>
					<tr>
<?php 		if ($primaryCol): ?>
						<td><?php echo htmlspecialchars ($rowid); ?></td>
<?php 		endif; ?>
<?php 		foreach ($row as $name => $value): ?>
						<td><?php echo htmlspecialchars ($value); ?></td>
<?php 		endforeach;

?>
						<td class="center"><?php echo saAdmin :: checkboxField ('selection', $rowid, $rowid); ?></td>
					</tr>
<?php 	endwhile; ?>
<?php else: ?>
					<tr>
						<td colspan="<?php echo count ($fields) + 1; ?>">No rows</td>
					</tr>
<?php endif; ?>
					<tr>
<?php foreach ($fields as $field): ?>
<?php 	if ($field -> name == 'rowid'): ?>
						<td>+</td>
<?php 	else: ?>
						<td><?php ?><?php echo saAdmin :: textField ("values[{$field -> name}]"); ?></td>
<?php 	endif; ?>
<?php endforeach; ?>
						<td></td>
					</tr>
				</table>
				<div class="submit">
					<?php echo saAdmin :: selectField ('action', array ('add-row' => 'Add row', 'drop-row' => 'Drop selected rows')) ?>
					<input type="submit" value="Apply …" />
				</div>
			</form>
<?php elseif ($action == 'structure'): ?>
<?php $columns = $table -> columns (); ?>
			<form action="<?php echo saAdmin :: queryString (array ('alias' => $database -> name, 'table' => $table -> name, 'do' => 'structure')); ?>" method="post">
				<table>
					<colgroup>
						<col />
						<col width="100" />
						<col width="55" />
						<col width="150" />
						<col width="50" />
					</colgroup>
					<caption>Columns (<?php echo count ($columns); ?>)</caption>
					<tr>
						<th>Name</th>
						<th>Type</th>
						<th>NULL</th>
						<th>Default value</th>
						<th></th>
					</tr>
<?php 	foreach ($columns as $name => $column): ?>
					<tr>
<?php 		if ($column -> pk): ?>
						<td class="primary-key"><?php echo htmlspecialchars ($name) ?></td>
<?php 		else: ?>
						<td><?php echo htmlspecialchars ($name) ?></td>
<?php 		endif; ?>
						<td><?php echo htmlspecialchars ($column -> type) ?></td>
						<td class="center"><?php echo ($column -> notnull ? 'No' : 'Yes') ?></td>
						<td><?php echo htmlspecialchars ($column -> dflt_value) ?></td>
						<td class="center"><?php echo saAdmin :: checkboxField ("selection[]", $column -> name) ?></td>
					</tr>
<?php 	endforeach; ?>
					<tr>
						<td><?php echo saAdmin :: textField ('name'); ?></td>
						<td><?php echo saAdmin :: selectField ('type', $types); ?></td>
						<td class="center"><?php echo saAdmin :: checkboxField ('notnull', '1'); ?></td>
						<td><?php echo saAdmin :: textField ('dflt_value'); ?></td>
						<td></td>
					</tr>
				</table>
				<div class="submit">
					<?php echo saAdmin :: selectField ('action', array ('add-column' => 'Add column', 'drop-column' => 'Drop selected columns')) ?>
					<input type="submit" value="Apply …" />
				</div>
			</form>
<?php elseif ($action == 'indexes'): ?>
<?php

$tableIndexes = array ();
$indexesResult = $database -> query ("
	SELECT name, sql
	FROM sqlite_master
	WHERE tbl_name = '" . saAdmin :: escapeString ($table -> name) . "' AND type = 'index'"
);

while ($index = $indexesResult -> fetch ()) {
	$columns = '';

	preg_match ('/\((.+)\)/', $index ['sql'], $match);

	if (isset ($match [1])) {
		$columns = explode (',', $match [1]);

		foreach ($columns as & $column)
			$column = trim ($column, " '");

		$columns = implode (', ', $columns);
	}

	if (stripos ($index ['sql'], 'UNIQUE') !== false) {
		$index ['type'] = 'unique';
	} else {
		$index ['type'] = 'index';
	}

	// Primary key
	if ($index ['sql'] == '') {
		list ($def) = $database -> query ("
			SELECT sql
			FROM sqlite_master
			WHERE type = 'table'
			AND name = '" . saAdmin :: escapeString ($table -> name) . "'
		") -> fetch (false);

		preg_match ('/PRIMARY\s+KEY\s*\(([^)]+)\)/', $def, $match);

		if (isset ($match [1])) {
			$columns = explode (',', $match [1]);

			foreach ($columns as & $column)
				$column = trim ($column, " '");

			$columns = implode (', ', $columns);
		}

		$index ['name'] = 'PRIMARY';
		$index ['type'] = 'unique';
	}

	$index ['columns'] = $columns;
	$tableIndexes [$index ['name']] = $index;
}

?>
			<form action="<?php echo htmlspecialchars ("?alias={$database -> name}&table={$table -> name}&do=indexes"); ?>" method="post">
				<table>
					<colgroup>
						<col />
						<col width="100" />
						<col />
						<col width="50" />
					</colgroup>
					<caption>Indexes (<?php echo count ($tableIndexes) ?>)</caption>
					<tr>
						<th>Name</th>
						<th>Type</th>
						<th>Columns</th>
						<th></th>
					</tr>
<?php if ($tableIndexes): ?>
<?php foreach ($tableIndexes as $name => $index): ?>
					<tr>
						<td><?php echo $index ['name']; ?></td>
						<td><?php echo strtoupper ($index ['type']);  ?></td>
						<td><?php echo $index ['columns'];  ?></td>
						<td class="center">
<?php if ($name != 'PRIMARY'): ?>
							<?php echo saAdmin :: checkboxField ("selection[]", $index ['name']) ?>
<?php endif; ?>
						</td>
					</tr>
<?php endforeach; ?>
<?php else: ?>
					<tr>
						<td colspan="4">No indexes</td>
					</tr>
<?php endif; ?>
					<tr>
						<td><?php echo saAdmin :: textField ('name'); ?></td>
						<td><?php echo saAdmin :: selectField ('index', $indexes); ?></td>
						<td><?php echo saAdmin :: textField ('columns'); ?></td>
						<td></td>
					</tr>
				</table>
				<div class="submit">
					<?php echo saAdmin :: selectField ('action', array ('add-index' => 'Add index', 'drop-index' => 'Drop selected indexes')) ?>
					<input type="submit" value="Apply …" />
				</div>
			<form>
<?php elseif ($action == 'options'): ?>
			<p>No options available yet</p>
<?php endif; ?>
