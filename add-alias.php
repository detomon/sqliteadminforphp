<?php

$versions = array (
	2 => 'SQLite',
	3 => 'SQLite3',
);

$validVersions = saSQLiteDBWrapper :: checkSystemVersions ();
$versions      = array_intersect_key ($versions, $validVersions);

if (empty ($_POST)) {
	end ($versions);
	$_REQUEST ['version'] = key ($versions);
}

?>
			<h2>Add alias</h2>
			<form action="./?do=add-alias" method="post">
				<table>
					<colgroup>
						<col width="100" />
						<col />
					</colgroup>
					<tr>
						<th valign="top"><label for="alias">Alias name</label></th>
						<td>
							<div><?php echo saAdmin :: textField ('alias') ?></div>
							<small>Displayed name in alias list</small>
						</td>
					</tr>
					<tr>
						<th valign="top"><label for="path">File path</label></th>
						<td>
							<div><?php echo saAdmin :: textField ('path') ?></div>
							<small>Path to database file<br />
							<strong>File and directories will be created if they not exist</strong><br />
							Replacement tokens:<br />
							%ROOT% (Web directory root)<br />
							%PARENT% (Parent directory of sqliteAdmin)</small>
						</td>
					</tr>
					<tr>
						<th valign="top"><label for="version">Version</label></th>
						<td>
							<div><?php echo saAdmin :: selectField ('version', $versions) ?></div>
							<small>Version only affects new files</small>
						</td>
					</tr>
				</table>
				<div class="submit">
					<input type="submit" value="Add" />
				</div>
			</form>
