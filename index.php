<?php

include 'classes.php';

set_error_handler ('saAdmin::errorHandler');

$action  = saAdmin :: formValue ('do');
$include = null;

switch ($action) {
	case 'login':
		$user     = saAdmin :: formValue ('user', 'post');
		$password = saAdmin :: formValue ('password', 'post');

		if (saUser :: login ($user, $password) == false)
			saAdmin :: addMessage ('Wrong username or password', 'err');

		break;
	case 'logout':
		saUser :: logout ();

		break;
	case 'add-alias':
		$include = 'add-alias.php';
		$alias   = saAdmin :: formValue ('alias');
		$path    = saAdmin :: formValue ('path');
		$version = saAdmin :: formValue ('version');

		if ($alias) {
			$absPath = saAdmin :: normalizeFilePath ($path);

			if ($absPath == null) {
				saAdmin :: addMessage ('File path is not valid', 'err');
			}
			else if (saAdmin :: createDirAtPath (dirname ($absPath)) == false) {
				saAdmin :: addMessage ('Could not create directory at specified path', 'err');
			}
			else {
				if (saDatabase :: addDatabase ($alias, $path, $version)) {
					saAdmin :: addMessage ('Alias has been added', 'msg');
					saAdmin :: redirect ('');
				}
			}
		}

		break;
	case 'add-table':
		$include = 'add-table.php';

		break;
	case 'add-row':
		$include = 'add-row.php';

		break;

}

$database = null;
$table    = null;

if ($alias = saAdmin :: formValue ('alias')) {
	$database = saDatabase :: databases ($alias);

	if ($database && $name = saAdmin :: formValue ('table')) {
		$table = $database -> tables ($name);
	}
}

if ($include == null) {
	if ($table) {
		$include = 'table.php';
	} else if ($database) {
		$include = 'database.php';
	}
}

$isLoggedIn = saUser :: isLoggedIn ();

if ($isLoggedIn)
	saAdmin :: addNavigationItem ('?do=logout', 'Logout', 100);

$wrapperClass [] = 'wrapper';

if (saUser :: isLoggedIn () == false)
	$wrapperClass [] = 'login';

$wrapperClass = implode (' ', $wrapperClass);

$content = null;

if ($include) {
	ob_start ();
	include $include;
	$content = ob_get_clean ();
}

$title = "";

if ($database) {
	$title .= "{$database -> name}";
}

if ($table) {
	$title .= " / {$table -> name}";
}

if ($title) {
	$title .= ' · ';
}

$title .= 'SQLiteAdminForPHP';

?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<meta charset="UTF-8" />
	<meta name="robots" content="noindex, nofollow" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/script.js" type="text/javascript"></script>
</head>
<body>
	<div class="<?php echo htmlspecialchars ($wrapperClass) ?>">
<?php if (saUser :: isLoggedIn ()): ?>
		<div class="nav">
			<h2>Databases</h2>
<?php 	if ($databases = saDatabase :: databases ()): ?>
			<ul>
<?php 		foreach ($databases as $name => $db): ?>
				<li><a href="<?php echo htmlspecialchars ("?alias={$name}") ?>" title="<?php echo htmlspecialchars ($name); ?>"><?php echo htmlspecialchars ($name); ?></a></li>
<?php 			if ($database && $database -> name == $name): ?>
<?php				$tables = $db -> tables (); ?>
<?php				if ($tables): ?>
				<li><ul>
<?php 					foreach ($db -> tables () as $tbl): ?>
					<li <?php if ($tbl == $table) echo 'class="active"'; ?>><a href="<?php echo htmlspecialchars ("?alias={$name}&table={$tbl -> name}") ?>" title="<?php echo htmlspecialchars ($tbl -> name); ?>"><?php echo htmlspecialchars ($tbl -> name) ?></a></li>
<?php 					endforeach; ?>
				</ul></li>
<?php 				endif; ?>
<?php 			endif; ?>
<?php 		endforeach; ?>
			</ul>
<?php 	else: ?>
			<div><strong>No alias</strong></div>
<?php 	endif; ?>
			<p><a href="?do=add-alias" class="button">Add alias …</a></p>
		</div>
<?php 	if ($items = saAdmin :: navigationItems ()): ?>
		<div class="nav sub-nav">
			<ul>
<?php 		foreach ($items as $href => $item): ?>
				<li class="<?php if ($item ['delta'] == 100) echo 'right '; ?><?php if ($item ['active']) echo 'active '; ?>"><a href="<?php echo htmlspecialchars ($href) ?>"><?php echo $item ['name']; ?></a></li>
<?php 		endforeach; ?>
			</ul>
		</div>
<?php 	endif; ?>
<?php foreach (saAdmin :: messages () as $type => $messages): ?>
		<ul class="messages <?php echo $type; ?>">
<?php 	foreach ($messages as $message): ?>
			<li><?php echo $message; ?></li>
<?php 	endforeach; ?>
		</ul>
<?php endforeach; ?>
		<div class="content">
<?php 	if ($content == null): ?>
			<p>Choose or add an alias</p>
<?php 	else: ?>
<?php 		echo $content; ?>
<?php 	endif; ?>
		</div>
<?php else: ?>
<?php foreach (saAdmin :: messages () as $type => $messages): ?>
		<ul class="messages <?php echo $type; ?>">
<?php 	foreach ($messages as $message): ?>
			<li><?php echo $message; ?></li>
<?php 	endforeach; ?>
		</ul>
<?php endforeach; ?>
		<div class="login-form">
			<h2>Login</h2>
			<form action="./" method="post">
				<table>
					<colgroup>
						<col width="90" />
						<col />
					</colgroup>
					<tr>
						<th><label for="name">User</label></th>
						<td><input type="text" name="user" id="name" /></td>
					</tr>
					<tr>
						<th><label for="password">Password</label></th>
						<td><input type="password" name="password" id="password" /></td>
					</tr>
				</table>
				<div class="submit">
					<input type="hidden" name="do" value="login" />
					<input type="submit" value="Login" />
				</div>
			</form>
		</div>
<?php endif; ?>
	</div>
</body>
</html>