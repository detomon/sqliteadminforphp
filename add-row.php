<?php

$rowcount = (int) saAdmin :: formValue ('rowcount');

$types = array (
	'INTEGER'       => 'INTEGER',
	'REAL'          => 'REAL',
	'TEXT'          => 'TEXT',
	'BLOB'          => 'BLOB',
	'AUTOINCREMENT' => 'AUTOINCREMENT',
);

$defaultValues = array (
	'NULL',
	'CURRENT_TIME',
	'CURRENT_DATE',
	'CURRENT_DATETIME',
);

$indexes = array (
	''            => '',
	'PRIMARY KEY' => 'PRIMARY',
);

function addRows ($database, $table)
{
	global $defaultValues;
	
	$values = array ();
	$fields = $table -> columns (); 
	$rowid  = saAdmin :: formValue ('__rowid');
	$ignore = saAdmin :: formValue ('__ignore');

	$database -> query ("BEGIN TRANSACTION");

	foreach ($fields as $field) {
		$column = saAdmin :: formValue ("v_{$field -> name}", 'post');

		foreach ($rowid as $id) {
			$id = (int) $id;

			if (isset ($ignore [$id]))
				continue;

			$value = $column [$id];

			if (in_array ($value, $defaultValues) == false) {
				if (substr ($value, 0, 1) == '$') {
					$value = substr ($value, 1);
				} else {
					if (substr ($value, 0, 2) == '\$')
						$value = substr ($value, 1);
					$value = "'" . saAdmin :: escapeString ($value) . "'";
				}
			}

			$fieldName = "'" . saAdmin :: escapeString ($field -> name) . "'";
			$values [$id][$fieldName] = $value;
		}
	}

	foreach ($values as $row) {
		$fields = implode (', ', array_keys ($row));
		$values = implode (', ', array_values ($row));
		$query = "INSERT INTO '" . saAdmin :: escapeString ($table -> name) . "' ($fields) VALUES ($values)";

		$database -> query ($query);
		$errstr = $database -> connection -> lastError ($errno);

		if ($errno) {
			saAdmin :: addMessage ("Could not insert row: $errstr", 'err');
			return false;
		}

		$lastInsertRowID = $database -> connection -> lastInsertRowID ();
		saAdmin :: addMessage ("Row with ID {$lastInsertRowID} has been inserted", 'msg');
	}

	$database -> query ("COMMIT TRANSACTION");

	return true;
}

if ($_POST) {
	if (saAdmin :: formValue ('__rowid', 'post')) {
		if (addRows ($database, $table))
			saAdmin :: redirect ("?alias={$database -> name}&table={$table -> name}");
	}
}

?>
			<h2>
				<a href="<?php echo saAdmin :: queryString (array ('alias' => $database -> name)) ?>"><?php echo htmlspecialchars ($database -> name); ?></a> /
				<a href="<?php echo saAdmin :: queryString (array ('alias' => $database -> name, 'table' => $table -> name)) ?>"><?php echo htmlspecialchars ($table -> name); ?></a> /
				<span>Add rows</span>
			</h2>
			<form action="<?php echo saAdmin :: queryString (array ('alias' => $database -> name, 'table' => $table -> name, 'do' => 'add-row')) ?>" method="post">
				<table>
					<colgroup>
						<col width="65" />
					</colgroup>
					<caption><?php echo saAdmin :: formValue ('name'); ?></caption>
					<tr>
<?php $fields = $table -> columns (); ?>
						<th>Ignore</th>
<?php foreach ($fields as $field): ?>
						<th><?php echo htmlspecialchars ($field -> name); ?></th>
<?php endforeach; ?>
					</tr>
<?php for ($i = 0; $i < $rowcount; $i ++): ?>
					<tr>
						<td><?php echo saAdmin :: hiddenField ("__rowid[$i]", $i) ?><?php echo saAdmin :: checkboxField ('__ignore', '1', $i); ?></td>
<?php foreach ($fields as $field): ?>
						<td><?php echo saAdmin :: textField ("v_{$field -> name}", 'text', $i) ?></td>
<?php endforeach; ?>
					</tr>
<?php endfor; ?>
				</table>
				<div class="submit">
					<?php echo saAdmin :: hiddenField ('name') ?>

					<?php echo saAdmin :: hiddenField ('rowcount') ?>

					<input type="submit" value="Add " />
				</div>
			</form>
